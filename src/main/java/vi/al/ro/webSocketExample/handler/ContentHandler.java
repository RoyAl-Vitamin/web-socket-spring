package vi.al.ro.webSocketExample.handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import vi.al.ro.webSocketExample.request.MessageHeader;
import vi.al.ro.webSocketExample.request.MessageType;
import vi.al.ro.webSocketExample.request.TextMessagePayload;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@Slf4j
@Component
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ContentHandler extends TextWebSocketHandler {

    ObjectMapper mapper;

    private final List<WebSocketSession> list = new ArrayList<>();

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) {
        log.info("received message = [{}]", new String(message.asBytes()));

        MessageHeader<TextMessagePayload> oMessage = null;
        try {
            oMessage = mapper.readValue(message.asBytes(),  new TypeReference<MessageHeader<TextMessagePayload>>() {});
        } catch (IOException e) {
            log.error("ERROR: ", e);
        }

        if (Objects.isNull(oMessage)) {
            log.error("Not valid message");
            return;
        }

        if (MessageType.ON_CONNECT.equals(oMessage.getType())) {
            if (connect(session)) {
                notificationConnected();
            }
            return;
        }
        if (MessageType.ON_DISCONNECT.equals(oMessage.getType())) {
            if (disconnect(session)) {
                notificationConnected();
            }
            return;
        }

        sendMessageToEveryoneButMe(session, message);
    }

    private boolean disconnect(WebSocketSession session) {
        return list.remove(session);
    }

    private boolean connect(WebSocketSession session) {
        if (list.contains(session)) {
            return false;
        }
        list.add(session);
        return true;
    }

    private void sendMessageToEveryoneButMe(WebSocketSession selfSession, TextMessage message) {
        boolean isNeedNotificationConected = false;
        Iterator<WebSocketSession> iterator = list.iterator();
        while (iterator.hasNext()) {
            WebSocketSession session = iterator.next();
            if (!session.isOpen()) {
                isNeedNotificationConected = true;
                iterator.remove();
                continue;
            }
            if (session.equals(selfSession)) {
                continue;
            }
            try {
                session.sendMessage(message);
            } catch (IOException e) {
                log.error("Произошла ошибка при отправке сообщения", e);
            }
        }
        if (isNeedNotificationConected) {
            notificationConnected();
        }
    }

    private void notificationConnected() {
        Iterator<WebSocketSession> iterator = list.iterator();
        while (iterator.hasNext()) {
            WebSocketSession session = iterator.next();
            if (!session.isOpen()) {
                iterator.remove();
                continue;
            }
            MessageHeader<Integer> message = new MessageHeader<>();
            message.setType(MessageType.CONNECT_COUNT);
            message.setData(list.size());
            try {
                session.sendMessage(new TextMessage(mapper.writeValueAsString(message)));
            } catch (IOException e) {
                log.error("Произошла ошибка при отправке сообщения", e);
            }
        }
    }
}


package vi.al.ro.webSocketExample.request;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TextMessagePayload {

    /**
     * Автор сообщения
     */
    String author;

    /**
     * Текст сообщения
     */
    String text;
}

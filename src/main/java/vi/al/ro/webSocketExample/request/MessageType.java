package vi.al.ro.webSocketExample.request;

public enum MessageType {

    ON_CONNECT,
    ON_DISCONNECT,
    MESSAGE,
    CONNECT_COUNT
}

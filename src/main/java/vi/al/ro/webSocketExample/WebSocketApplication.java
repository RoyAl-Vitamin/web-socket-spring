package vi.al.ro.webSocketExample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebSocketApplication {

	// https://spring.io/guides/gs/messaging-stomp-websocket/
	public static void main(String[] args) {
		SpringApplication.run(WebSocketApplication.class, args);
	}
}

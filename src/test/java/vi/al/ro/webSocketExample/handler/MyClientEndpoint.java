package vi.al.ro.webSocketExample.handler;

import lombok.extern.slf4j.Slf4j;

import javax.websocket.*;
import java.io.IOException;

@Slf4j
@ClientEndpoint
public class MyClientEndpoint {

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Connected to endpoint: " + session.getBasicRemote());
        try {
            String name = "Duke";
            System.out.println("Sending message to endpoint: " + name);
            session.getBasicRemote().sendText(name);
        } catch (IOException ex) {
            log.error("ERROR: ", ex);
        }
    }

    @OnMessage
    public void processMessage(String message) {
        log.info("MESSAGE: {}", message);
//        Client.messageLatch.countDown();
    }

    @OnError
    public void processError(Throwable t) {
        t.printStackTrace();
    }
}

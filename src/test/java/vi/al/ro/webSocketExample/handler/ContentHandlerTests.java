package vi.al.ro.webSocketExample.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import vi.al.ro.webSocketExample.request.MessageHeader;
import vi.al.ro.webSocketExample.request.MessageType;
import vi.al.ro.webSocketExample.request.TextMessagePayload;

import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
@SpringBootTest
@WebAppConfiguration
public class ContentHandlerTests {

//    @Autowired
//    TextWebSocketHandler textWebSocketHandler;

    @Autowired
    ObjectMapper mapper;

    private static final String ADDRESS = "ws://localhost:8080/chat/";
//    private static final String ADDRESS = "ws://localhost:8080/";

    @Test
    void test1() {
        try {
            WebSocketContainer container = ContainerProvider.getWebSocketContainer();
            container.connectToServer(MyClientEndpoint.class, URI.create(ADDRESS));
//            messageLatch.await(100, TimeUnit.SECONDS);
        } catch (DeploymentException | IOException ex) {
            log.error("ERROR: ", ex);
            fail();
        }
    }

    @Test
    void test0() {
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            log.error("ERROR:", e);
            fail();
        }
        WebSocketClient webSocketClient = new StandardWebSocketClient();
        WebSocketSession clientSession = null;
        try {
            clientSession = webSocketClient.doHandshake(
//                    textWebSocketHandler,
                        new MyTextWebSocketHandler(),
                    new WebSocketHttpHeaders(),
                    URI.create(ADDRESS))
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            log.error("ERROR:", e);
            fail();
        }
        try {
            MessageHeader<TextMessagePayload> onConnectMessage = new MessageHeader<>();
            onConnectMessage.setType(MessageType.ON_CONNECT);
            clientSession.sendMessage(new TextMessage(mapper.writeValueAsString(onConnectMessage)));
            MessageHeader<TextMessagePayload> textMessage = new MessageHeader<>();
            textMessage.setType(MessageType.MESSAGE);
            TextMessagePayload payload = new TextMessagePayload();
            payload.setAuthor("author");
            payload.setText("Hello!");
            textMessage.setData(payload);
            clientSession.sendMessage(new TextMessage(mapper.writeValueAsString(onConnectMessage)));
        } catch (IOException e) {
            log.error("ERROR:", e);
            fail();
        }
    }

    static class MyTextWebSocketHandler extends TextWebSocketHandler {
        @Override
        protected void handleTextMessage(WebSocketSession session, TextMessage message) {
            log.info(message.getPayload());
        }
    }
}
